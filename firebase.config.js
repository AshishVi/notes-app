import { initializeApp } from "firebase/app";
import { getDoc, collection, getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCXvJTpM1ImMs2na3Rf69nvv9p-cJiXoQA",
  authDomain: "notes-404207.firebaseapp.com",
  projectId: "notes-404207",
  storageBucket: "notes-404207.appspot.com",
  messagingSenderId: "957660036886",
  appId: "1:957660036886:web:6f7a3c8aa301a689941a37",
  measurementId: "G-KRTNK6QQ0J",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);


