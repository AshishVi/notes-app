import { useContext, useMemo } from "react";
import { Context } from "../context/DataContext";
import { Link, useParams } from "react-router-dom";
import { LuClipboardEdit } from "react-icons/lu";

const NotePage = () => {
  const { notes, getDate } = useContext(Context);
  const { id } = useParams();

  const note = useMemo(() => {
    return notes.find((value) => value.id === id).data();
  }, [notes, id]);

  return (
    <div className="p-2">
      {note && (
        <div>
          <h1 className="text-2xl font-bold text-secondary mt-3">
            {note.title}
          </h1>
          <h3 className="text-accent/50">{getDate(note.dateTime.seconds)}</h3>
          <p className="mt-3">{note.content}</p>
        </div>
      )}
      <Link to={`/edit/${id}`} className="btn btn-lg btn-circle btn-secondary fixed bottom-10 right-8 shadow-xl shadow-secondary/20">
        <LuClipboardEdit className="h-6 w-6" />
      </Link>
    </div>
  );
};

export default NotePage;
