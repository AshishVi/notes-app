import { useState, useContext } from "react";
import { Context } from "../context/DataContext";

const NewNote = () => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const { submitNotes } = useContext(Context);

  const handleSubmit = (e) => {
    e.preventDefault();
    submitNotes(title, content);
  };

  return (
    <form
      className="form-control flex flex-col h-[calc(100%-100px)]"
      onSubmit={(e) => handleSubmit(e)}
    >
      <input
        className="input !outline-none text-center text-xl mb-3"
        type="text"
        required
        placeholder="Note title"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      <textarea
        className="textarea textarea-secondary flex-grow min-h-[100px] text-md"
        placeholder="Note body"
        required
        value={content}
        onChange={(e) => setContent(e.target.value)}
      ></textarea>
      <button
        type="submit"
        className="btn btn-secondary max-w-fit font-extrabold px-6 m-auto mt-5"
      >
        Submit
      </button>
    </form>
  );
};

export default NewNote;
