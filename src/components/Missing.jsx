import { Link } from "react-router-dom";

const Missing = () => {
  return (
    <div className=" h-[calc(100vh-205px)] grid place-content-center">
      <h1 className="text-7xl font-bold text-center">Oops!</h1>
      <p className="mt-3 text-lg">Page not found you are looking for.</p>
      <Link to="/" className="btn btn-secondary btn-outline w-fit mt-5 m-auto">Back to Home</Link>
    </div>
  )
}

export default Missing