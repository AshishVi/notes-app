import { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { Context } from "../context/DataContext";
import { BiTrashAlt } from "react-icons/bi";
import { PiPushPin, PiPushPinFill } from "react-icons/pi";

const Note = ({ note, noteId }) => {
  const { deleteNote, getDate, pinNote } = useContext(Context);
  const [isPinned, setIsPinned] = useState(note.pinned);

  const handlePinNote = async (e) => {
    e.preventDefault();
    setIsPinned(!isPinned);
    await pinNote(noteId, !isPinned);
  };

  return (
    <div className="card card-bordered bg-base-100 shadow-lg shadow-neutral card-compact">
      <Link to={`/note/${noteId}`}>
        <div className="card-body h-[225px]">
          <h2 className="card-title text-secondary text-[1rem] sm:text-md leading-tight">
            {note?.title}
          </h2>
          <p className="overflow-hidden flex-grow">
            {note?.content}
          </p>
          <div className="flex justify-end mt-4">
            <p className="text-sm">
              {getDate(note?.dateTime?.seconds)}
            </p>
            <div className="card-actions justify-end">
              <button
                onClick={(e) => {
                  handlePinNote(e);
                }}
              >
                {isPinned ? (
                  <PiPushPinFill className="h-5 w-5" />
                ) : (
                  <PiPushPin className="h-5 w-5" />
                )}
              </button>
              <button
                onClick={(e) => {
                  e.preventDefault();
                  deleteNote(noteId);
                }}
              >
                <BiTrashAlt className="h-5 w-5" />
              </button>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default Note;
