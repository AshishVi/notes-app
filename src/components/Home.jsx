import { useContext, useState } from "react";
import ReactPaginate from "react-paginate";
import { Context } from "../context/DataContext";
import Note from "./Note";
import SearchForm from "./SearchForm";

const Home = () => {
  const { searchResults } = useContext(Context);
  const [currentPage, setCurrentPage] = useState(0);

  const pageSize = 6;
  const pageCount = Math.ceil(searchResults.length / pageSize);
  const currentData = searchResults.slice(
    currentPage * pageSize,
    (currentPage + 1) * pageSize
  );

  return (
    <>
      <SearchForm />
      <div className="flex-grow grid gap-2 grid-cols-2 p-3 place-content-start">
        {currentData.map((note) => (
          <Note key={note?.id} noteId={note?.id} note={note?.data()} />
        ))}
      </div>
      <div className="m-auto py-7">
        <ReactPaginate
          className="join hover:bg-secondary-focus"
          pageClassName="join-item btn btn-sm bg-base-content text-base-100 hover:text-base-content"
          previousLinkClassName="join-item btn btn-sm  bg-base-content text-base-100 hover:bg-secondary-focus"
          nextLinkClassName="join-item btn btn-sm bg-base-content text-base-100 hover:bg-secondary-focus"
          activeClassName="btn-active bg-secondary-focus"
          previousLabel={"Prev"}
          nextLabel={"Next"}
          breakLabel={"..."}
          pageCount={pageCount}
          forcePage={currentPage}
          onPageChange={(event) => {
            setCurrentPage(event.selected);
          }}
          renderListItem={(item) => (
            <a className={item.isActive ? "active" : ""} onClick={item.onClick}>
              {item.page + 1}
            </a>
          )}
        />
      </div>
    </>
  );
};

export default Home;
