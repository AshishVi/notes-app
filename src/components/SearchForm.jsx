import { useContext, useEffect, useState } from "react";
import { Context } from "../context/DataContext";

const SearchForm = () => {
  const [searchText, setSearchText] = useState("");
  const { notes, setSearchResults } = useContext(Context);

  useEffect(() => {
    const filteredNotes = notes.filter((note) =>
      note.data().title.toLowerCase().includes(searchText.toLocaleLowerCase())
    );
    setSearchResults(filteredNotes);
  }, [searchText, notes]);

  return (
    <div className="form-control w-full max-w-lg px-3 m-auto">
      <div className="input-group">
        <button className="btn btn-square w-[65px] bg-secondary text-secondary-content hover:bg-secondary">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
            />
          </svg>
        </button>
        <input
          type="text"
          placeholder="Search…"
          value={searchText}
          onChange={({ target }) => setSearchText(target.value)}
          className="input input-bordered w-full !outline-none text-lg"
        />
      </div>
    </div>
  );
};

export default SearchForm;
