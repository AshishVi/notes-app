import { useState, useContext, useEffect, useMemo } from "react";
import { Context } from "../context/DataContext";
import { useParams } from "react-router-dom";

const EditNote = () => {
  const [editedTitle, setEditedTitle] = useState("");
  const [editedContent, setEditedContent] = useState("");
  const { editNotes, notes } = useContext(Context);
  const { id } = useParams();

  const note = useMemo(() => {
    return notes.find((value) => value.id === id).data();
  }, [notes, id]);

  useEffect(() => {
    setEditedTitle(note.title);
    setEditedContent(note.content);
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    editNotes(id, editedTitle, editedContent);
  };

  return (
    <form
      className="form-control flex flex-col h-[calc(100%-100px)]"
      onSubmit={(e) => handleSubmit(e)}
    >
      <input
        className="input !outline-none text-center text-xl mb-3"
        type="text"
        required
        placeholder="Note title"
        value={editedTitle}
        onChange={(e) => setEditedTitle(e.target.value)}
      />
      <textarea
        className="textarea textarea-secondary flex-grow min-h-[100px] text-md"
        placeholder="Note body"
        required
        value={editedContent}
        onChange={(e) => setEditedContent(e.target.value)}
      ></textarea>
      <button
        type="submit"
        className="btn btn-secondary max-w-fit font-extrabold px-6 m-auto mt-5"
      >
        Submit
      </button>
    </form>
  );
};

export default EditNote;
