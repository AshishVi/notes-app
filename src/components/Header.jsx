import { Link } from "react-router-dom";
import AddBtn from "./AddBtn";

const Header = () => {

  return (
    <div className="navbar rounded mb-5 shadow-lg sticky top-0 z-10 bg-base-100">
      <div className="flex-1">
        <Link to="/" className="btn btn-ghost text-secondary hover:bg-base-100 normal-case text-2xl">
          Notes
        </Link>
      </div>
      <AddBtn />
    </div>
  );
};

export default Header;
