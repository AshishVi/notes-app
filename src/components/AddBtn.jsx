import { FaPlus } from "react-icons/fa";
import { Link } from "react-router-dom";

const AddBtn = () => {
  return (
    <Link
      to="/newnote"
      className="btn btn-sm btn-square sm:btn-md btn-secondary shadow-xl shadow-secondary/20"
    >
      <FaPlus className="h-5 w-5" />
    </Link>
  );
};

export default AddBtn;
