import { useState, useEffect, createContext, useCallback, useMemo } from "react";
import { Timestamp, addDoc, collection, deleteDoc, doc, setDoc, onSnapshot } from "firebase/firestore";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import { db } from "../../firebase.config";

export const Context = createContext();

export const DataContext = (props) => {
  const [notes, setNotes] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const navigate = useNavigate();

  const deleteNote = useCallback(
    async (noteId) => {
      const documentRef = doc(db, "notes", noteId);
      await deleteDoc(documentRef);
    },
    [db, notes]
  );

  const getDate = useMemo(() => {
    return (value) => {
      const dateTime = moment(value * 1000).format("MMM DD, YYYY");
      return dateTime;
    };
  }, []);

  const editNotes = useCallback(
    async (id, title, content) => {
      await setDoc(
        doc(db, "notes", id),
        {
          title,
          content,
        },
        { merge: true }
      );
      navigate("/");
    },
    [db, notes]
  );

  const pinNote = useCallback(
    async (id, pin) => {
      await setDoc(
        doc(db, "notes", id),
        {
          pinned: pin,
        },
        { merge: true }
      );
    },
    [db, notes]
  );

  useEffect(() => {
    const unsubscribe = onSnapshot(collection(db, "notes"), (snapshot) => {
      const docs = snapshot.docs;
      setNotes(docs);
    });

    return unsubscribe;
  }, []);

  const submitNotes = async (title, content) => {
    const noteCollection = collection(db, "notes");
    await addDoc(noteCollection, {
      title,
      content,
      pinned: false,
      dateTime: Timestamp.fromDate(new Date()),
    });

    navigate("/");
  };

  const fetchNoteById = (id) => {
    const note = notes.find((value) => value.id === id);
    return note;
  };

  return (
    <Context.Provider
      value={{
        notes,
        setNotes,
        submitNotes,
        deleteNote,
        fetchNoteById,
        getDate,
        editNotes,
        searchResults,
        setSearchResults,
        pinNote
      }}
    >
      {props.children}
    </Context.Provider>
  );
};
