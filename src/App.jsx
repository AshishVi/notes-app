import { BrowserRouter, Routes, Route } from "react-router-dom";

import Header from "./components/Header";
import Home from "./components/Home";
import NewNote from "./components/NewNote";
import { DataContext } from "./context/DataContext";
import NotePage from "./components/NotePage";
import EditNote from "./components/EditNote";
import Missing from "./components/Missing";

const App = () => {
  return (
    <BrowserRouter>
      <div className="flex flex-col m-auto max-w-[900px] h-screen p-2">
        <DataContext>
          <Header />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/newnote" element={<NewNote />} />
            <Route path="note/:id" element={<NotePage />} />
            <Route path="edit/:id" element={<EditNote />} />
            <Route path="*" element={<Missing />} />
          </Routes>
        </DataContext>
      </div>
    </BrowserRouter>
  );
};

export default App;
